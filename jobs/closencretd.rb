require 'net/https'
require 'uri'
require 'json'
require 'active_support/core_ext/numeric/time'



SCHEDULER.every '30s' do

uri = URI.parse("https://jira.services.kambi.com/rest/api/latest/search?jql=%28project+%3D+%22Customer+Support%22%29++AND+type+%3D++%22IT+Issue%22&tempMax=1000")

http = Net::HTTP.new(uri.host, uri.port)

http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE
request = Net::HTTP::Get.new(uri.request_uri)
request.basic_auth("soaptester", "password")
response = http.request(request)

jsn = JSON.parse(response.body) 

  grpcs = jsn["issues"]

  closcs = []
  inprogscs = []
  pendingcs = []

    grpcs.each do |cl|
      kys = cl["key"]
      crt = cl["fields"]["created"]
      stat = cl["fields"]["status"]["name"]
      crtd = DateTime.parse(crt)
      dtecrtd = crtd.month
      omon = Time.now 
      mont = omon.month
    
      if dtecrtd == mont #stat == "In Progress" || "Pending" 
        case stat
          when "In Progress" then
            #puts "status #{stat} #{kys} #{dtecrtd}"
             inprogscs << "#{kys}" 
          when "Pending" then
            #puts "status #{stat} #{kys} #{dtecrtd}"
             pendingcs << "#{kys}" 
          when "Closed" then
            #puts "status #{stat} #{kys} #{dtecrtd}"
             closcs << "#{kys}" 
        end
      end
    end

    csclose = closcs.count
    csinprog = inprogscs.count
    cspending = pendingcs.count
    createdcs = csinprog.to_i + cspending.to_i

    lastcl = csclose 
    lastcrt = createdcs 


    send_event( 'closedcs', { current: csclose, last: lastcl } )
    send_event( 'createdcs', { current: createdcs, last: lastcrt } )

end
