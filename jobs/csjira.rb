require 'net/https' 
require 'uri' 
require 'json'


SCHEDULER.every '10s' do

uri = URI.parse("https://jira.services.kambi.com/rest/api/latest/search?jql=%28project+%3D+%22Customer+Support%22%29++AND+type+%3D++%22IT+Issue%22+AND+status+not+in+%28Closed%29&tempMax=1000")

http = Net::HTTP.new(uri.host, uri.port)
http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE
request = Net::HTTP::Get.new(uri.request_uri)
request.basic_auth("soaptester", "password")
response = http.request(request)

i = 1
cust = []
unack = Hash.new()
ack = Hash.new()
pend = Hash.new()

    jsonf = JSON.parse(response.body) 

    js = jsonf["issues"]
    js.each do |cs|
      sel = cs["fields"]["summary"]
      keys = cs["key"]
      create = cs["fields"]["created"]
      stats = cs["fields"]["status"]["name"]
      #pp "#{keys} created:#{create} details: #{sel}"
      if stats == "Open"      
        unack["#{i}"] = { label: "#{keys}", value: "#{stats}" }
        cust << keys
        i += 1
      elsif stats == "In Progress"
        ack["#{i}"] = { label: "#{sel}".slice!(0..44)+"...", value: "#{keys}" }
        i += 1
      elsif stats == "Pending"
        pend["#{i}"] = { label: "#{sel}".slice!(0..44)+"...", value: "#{keys}" }
        i += 1
      end
    end
    
    cst = cust.count
  
      if cst != 0 then status = "average" else status = "ok" end
      send_event( 'csopen', { items: unack.values, status: status } )
      send_event( 'csinprog', { items: ack.values } )
      send_event( 'cspend', { items: pend.values } )

end
