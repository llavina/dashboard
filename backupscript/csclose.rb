require 'net/https'
require 'uri'
require 'json'
require 'active_support/core_ext/numeric/time'

SCHEDULER.every '25s' do

uri = URI.parse("https://jira.services.kambi.com/rest/api/latest/search?jql=%28project+%3D+%22Customer+Support%22%29++AND+type+%3D++%22IT+Issue%22+AND+status+in+%28Closed%29&tempMax=1000")

http = Net::HTTP.new(uri.host, uri.port)

http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE
request = Net::HTTP::Get.new(uri.request_uri)
request.basic_auth("soaptester", "password")
response = http.request(request)

cljson = JSON.parse(response.body) 
close = cljson["issues"]
numcl = []

close.each do |cl|
  fld = cl["fields"]["summary"]
  kys = cl["key"]
  cret = cl["fields"]["created"]
  created = DateTime.parse(cret)
  datecreated = created.month
  onemon = Time.now 
  mon = onemon.month
  if datecreated == mon
    numcl << "#{kys}"
  end
end

  clsnum = numcl.count
  lastnum = clsnum
  currentcs = clsnum 

  send_event( 'closedcs', { current: currentcs, last: lastnum } )

end



